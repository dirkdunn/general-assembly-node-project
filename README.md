# General Assembly Instructor Code Challenge
Below are the technical and user guidelines in regards to using the Easy Movie Searcher. At the bottom, we will analyze why using a framework over doing this in vanilla JS can help us streamline the process greatly.

### Start The App
* Go to your computers terminal app.
* Navgate to the root directory of the application node-backend-starter-code on the termial
*  Type ``` npm start ```
*  Go to http://localhost:3000 in your browser

Or alternatively you can visit here on heroku: http://mighty-crag-44942.herokuapp.com/

### Using the app
The easy movie searcher just so happens to be easy to use!

* Simply type in the name of the movie, and then click the "Find Movie" button to get a list of movies similar to what you are searching for.

* If you would like to come back to the movie, simply click the start next to the title, and it will be added to your favorites widget in the bottom right corner.

* You may also remove the movie from your favorites by again clicking on the start, to deselect the movie.

That's It!

##### Note to General assembly:
It seems that on heroku, images are showing up as blank. After researching the issue, it seems that IMDB does not permit direct linking of their images. Since there was no DB in this project, only a JSON file, You will need to view the project on localhost in order to see movie images.

See here: http://stackoverflow.com/questions/11044010/imdb-poster-url-returns-referral-denied

##### Why use a framework?
As you can see by viewing the source code, doing things in vanilla JS requires us to account for many details of the application. This works well when it comes to smaller applications, but as applications scale(get bigger), having a framework to abstract many of the details (E.G. do much of the tedious repititive work for us ) is often necessary.

For example note the $ajax function under helpers.js. Within that function is code that is required every time to call an ajax function. If I were to write all that code out every time I used AJAX in main.js, 
what a nightmare!

Also, as you can see in this example, most of our application logic is within main.js. When using a framework such as AngularJS, we are able to split our logic up into seperate parts of the application, allowing us to better understand what each part does. 

Although there is certainly no replacing well written code, a framework which takes our existing code, and breaks it into a Model/View/Controller (or in Angulars case Model/View/Whatever) can help us better understand what our application is doing as it gets larger and larger.
