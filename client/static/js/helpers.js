/* Helper Functions mini library

   Sometimes writing some functions to help simplify a task that may be used multiple times
   is a good practice, it is also considered DRY (DO NOT REPEAT YOURSELF).

   For example, using AJAX in vanilla JS isn't the most pretty looking thing, especially
   when we have libraries like jQuery that simplify the process.

   Writing out a helper function for this so that we can avoid typing standard AJAX code over
   and over again will make our main code look cleaner, an allow us to focus on more important
   aspects of the application.

*/

window.helpers = new function(){

	this.$ajax = function (method,destination,cb,async,data){
		console.log('data: ', data);
		async = async || true;
		var method = method || undefined, destination = destination || undefined, cb = cb || undefined,
			parameterTypesCorrect = typeof method === "string" && typeof destination === "string" && typeof cb === "function",
			data = data || {};
		
		if( parameterTypesCorrect ){
		  var req = new XMLHttpRequest();
		  req.onload = function (event) {
				// console.log('event',event);
		        if(event.target.status === 200){
		            cb(event.target.response)
		        }
			};

		  req.open(this.trim(method), this.trim(destination), async);
		  if(/put/i.test(method)){
		  	req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		  }	 
		   
		  req.send(data);

		} else  {
			console.error("Wrong parameter types, $ajax(method[string],destination[string],cb[function])");
		}
	};

	this.addClass = function(el,className){
		if (typeof el === 'string')
			el = document.querySelector(el);

		if (el.classList)
		  el.classList.add(className);
		else
		  el.className += ' ' + className;
	};

	this.removeClass = function(el,className){
		if (typeof el === 'string')
			el = document.querySelector(el);

		if (el.classList)
		  el.classList.remove(className);
		else
		  el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	};

	this.trim = function(str){
		return str.replace(/^\s+|\s+$/,"");
	};

	this.toggle = function(el){		
		el = typeof el === 'string' ? document.querySelector(el) : el;

		var elementIsHidden = el.style.display === 'none';
		if(elementIsHidden){
			el.style.display = 'block';
		} else {
			el.style.display = 'none'
		}
	};

};