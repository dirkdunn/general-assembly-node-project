/* Main.js
	For this project, we will have all of our main logic in this JS file.

	Each thing the app does will be split into it's own function, this creates more readable code.
*/

(function(h){

	/* Here we have functions attached to the window object in the browser, therefore
	making them accessible to all over scopes. In this case, we are doing this so our 
	"onclick" html attributes can call them.

	We use the onclick attribute in HTML for our dynamic data, because our event handlers cannot see
	them when the browser is initially loaded. */

	/*Grab the movie title from the data-title attribute of the particular "More Info" button you click, and 
	pass it to the getDetails function. */
	window.moreInfo = function(e){
		var title = e.getAttribute('data-title');
		console.log('title: ',title);
		getDetails(title);
	};

	/* When star is clicked, either add the movie to favorites, or delete it, depending on whether or not
	it is already favorited. */
	window.addToFavorites = function (e){
		var title = e.getAttribute('data-title');
		h.addClass(e,'favorited');

		if(movieIsFavorited(title) === false){
			e.style.backgroundPosition = '0px 15px';
			var params = ["?t=",title.replace(/\s/g,"+"),"&y=&plot=full&r=json"].join(''),
				fullUrl = ['http://www.omdbapi.com/',params].join('');

			h.$ajax('get', fullUrl, function(res){
				if( res ){
					updateFavorites( JSON.parse(res) );
				}
			});
		} else {
			e.style.backgroundPosition = '0px 0px';
			var deleteUrl = ['/favorites/',title].join('');
			h.$ajax('delete', deleteUrl, function(res){
					// remove from favorites
			});
			removeMovieFromFavorites(title);
		}
	};

	/* grab the favorite when you click it within the widget, and pass it to the search function. */
	window.retrieveFavorite = function (e){
		var title = e.innerText;
		searchForFavorite(title);
	};

	/* Just to shorten the process of writing document.querySelector, or document,querySelectorAll,
	I have these two helper functions within this scope. */
	function qs(queryString){
		return document.querySelector(queryString);
	};

	function qsa(queryString){
		return document.querySelectorAll(queryString);
	};

	/* The wobble animation on the search button */
	function searchButtonAnimation(){
		h.addClass('#search input.submit','wobble');

		var wobbleRestart = setInterval(function(){
			h.removeClass('#search input.submit','wobble');
			clearInterval(wobbleRestart);
		},1000);
	};

	/* Lock/Unlock the browser from scrolling, used when we show the modal. */
	function lockBrowser(){
		qs('html.modal-lock').style.overflow = 'hidden';
	};

	function unlockBrowser(){
		qs('html.modal-lock').style.overflow = 'visible';
	};

	/* Make an ajax request to get the title of a particular movie */
	function getDetails(title){
		// http://www.omdbapi.com/?t=title&y=&plot=full&r=json
		var params = ["?t=",title.replace(/\s/g,"+"),"&y=&plot=full&r=json"].join(''),
			fullUrl = ['http://www.omdbapi.com/',params].join('');

		h.$ajax('get', fullUrl, function(res){
			console.log('ajax res: ', JSON.parse(res));
			if( res ){
				createMovieDetailsModal( JSON.parse(res) );
			}
		});
	};

	/* Create the inner HTML of the favorites widget and replace it. */
	function updateFavorites(movieDetails){
		var data = {};

		for(var key in movieDetails){
			if( movieDetails.hasOwnProperty(key) && key !== 'Poster'){
				var val = movieDetails[key];
				data[key] = val;
			}
		}

		h.$ajax('PUT','/favorites',function(res){
			console.log('put returned: ', res);
			createFavoritesHtml();
		},true,JSON.stringify(data));

	};

	/* Create the actual html for the list of favorites within the favorites bar on the bottom left,
	grabs the favorites with ajax, then iterates through and adds the title as it's own <li> element. */
	function createFavoritesHtml(){
		h.$ajax('get','/favorites',function(res){
			var favorites = JSON.parse(res),
				flist = qs('#favorites ul.favorite-list');
				console.log(favorites)

			flist.innerHTML = '';

			for(key in favorites){
				if(favorites.hasOwnProperty(key)){
					var li = document.createElement('li');
					li.setAttribute('onclick','retrieveFavorite(this)')
					li.innerHTML = favorites[key].Title;
					flist.appendChild(li);
				}
			}

			if( Object.keys(favorites).length === 0 )
				flist.innerHTML = 'You have no favorites!';
		});
	};

	/* Create the html that will go into the modal, and then append it inside. 
	Then shows the modal. */
	function createMovieDetailsModal(movieDetails){
		var posterImageDoesNotExist = new RegExp("N\/A").test(movieDetails.Poster);

		qs('#movieinfomodal .movie-info').innerHTML = [
		'<img src="',posterImageDoesNotExist  ? 'img/blank.png' : movieDetails.Poster,'"/>',
		'<h3>',movieDetails.Title,'</h3>'
		].join('');

		for(var key in movieDetails){
			if( movieDetails.hasOwnProperty(key) && key !== 'Poster'){
				var val = movieDetails[key],
					row = document.createElement('tr'),
					title = document.createElement('td'),
					content = document.createElement('td');
				
				console.log(key);
				title.innerHTML = key;
				content.innerHTML = val;

				row.appendChild(title);
				row.appendChild(content);

				qs('#movieinfomodal .movie-info').appendChild(row);	
			}
		}

		lockBrowser();
		h.toggle('#movieinfomodal');
		h.toggle('#modalbackground');
	};

	/* Creates the HTML that is within each movie item that shows 
	when you search (E.G. the moreinfo button, the star, etc.) */
	function createMovieListHtml(movies){

		if( Array.isArray(movies) ){
			qs('#results .movie-list').innerHTML = '';
			var ul = document.createElement('ul');

			for(var i=0,l=movies.length;i<l;i++){

				var movie = movies[i];
				var movieContainer = document.createElement('li');
				var aboutMovie = [
					"",
					"<h4> <div class=\"star ",movieIsFavorited(movie.Title) ? "favorited" : "", 
					"\" data-title=\"",movie.Title,"\" onclick=\"addToFavorites(this)\"></div> ",movie.Title,"</h4>",
					"<p>Year: ",movie.Year,"</p>",
					"<img src=\"",new RegExp("N\/A").test(movie.Poster) ? "img/blank.png" : movie.Poster ,"\">",
					"<p><button class=\"moreinfo\" data-title=\"",movie.Title,"\" onclick=\"moreInfo(this)\">More Info</button></p>"
				].join('');

				movieContainer.innerHTML = aboutMovie;
				ul.appendChild(movieContainer);
			}

			qs('#results .movie-list').appendChild(ul);

		} else {
			console.error('movies must be an array!');
		}
	};

	/* A helper function that returns a boolean that tells us whether or not the movie is favorited. */
	function movieIsFavorited(title){
		var favorited = false;
		[].forEach.call(qsa('div#favorites ul.favorite-list li'),function(li){
			if(li.innerText === title){
				favorited = true;
			}
		});

		return favorited;
	};

	/* Removes the given movie title passed as a parameter from favorites */
	function removeMovieFromFavorites(title){
		console.log('called!');
		var favorited = false;
		[].forEach.call(qsa('div#favorites ul.favorite-list li'),function(li){
			if(li.innerText === title){
				li.style.display = 'none';
			}
		});

		if(qsa('div#favorites ul.favorite-list li').length === 1 && qs('div#favorites ul.favorite-list li').style.display === 'none'){
			qs('#favorites ul.favorite-list').innerHTML = 'You have no favorites!'
		}

		return favorited;
	};

	/* This function is triggered when you click one of your favorites from the list.
	Grabs the title of the movie and then passes it to the createMovieListHtml function, just
	as if we were searching for it. */

	function searchForFavorite(title){

		var submitUrl = 'http://www.omdbapi.com/',
			titlesearch = title,
			queryParams = ['?s=',titlesearch.replace(/\s/g,'+'),'&r=json'].join(''),
			fullUrl = [submitUrl,queryParams].join('');

		qs('#search .movie-title').value = title;

		//console.log('fullUrl: ',fullUrl);

		h.$ajax('get',fullUrl,function(res){
			console.log(JSON.parse(res));
			createMovieListHtml(JSON.parse(res).Search);
		});
					
	}

	/* Set */
	function setHandlers(){

		// Open Favorites
		qs('#favorites').addEventListener('click',function(e){
			var favorites = qs('#favorites .inner');
			if(favorites.style.display === 'none')
				favorites.style.display = 'block';
			else
				favorites.style.display = 'none';
		});

		qs('#favorites .inner').addEventListener('click',function(e){
			e.stopPropagation();
		});

		// CloseModal
		qs('#movieinfomodal button.close-modal').addEventListener('click',function(e){
			h.toggle('#movieinfomodal');
			h.toggle('#modalbackground');
			unlockBrowser();
		}); 
		
		// Search for movie via form submission (Filter Search checkbox is not checked)
		qs('#search .movie-search').addEventListener('submit',function(e){
			e.preventDefault();

			if(qs('#filter-search').checked === false){
				var submitUrl = e.target.action,
				titlesearch = h.trim(this.querySelector('input.movie-title').value),
				queryParams = ['?s=',titlesearch.replace(/\s/g,'+'),'&r=json'].join(''),
				fullUrl = [submitUrl,queryParams].join('');

				h.$ajax('get',fullUrl,function(res){
					console.log(JSON.parse(res));
					createMovieListHtml(JSON.parse(res).Search);
				});
			}
			
		});

		// Search for movie via input event (Filter Search checkbox is checked)
		// more info on input event: https://developer.mozilla.org/en-US/docs/Web/Events/input
		qs('#search .movie-search .movie-title').addEventListener('input',function(e){
			e.preventDefault();
			if(qs('#filter-search').checked === true){
				var submitUrl = h.trim(qs('#search .movie-search').action),
					titlesearch = h.trim(this.value),
					queryParams = ['?s=',titlesearch.replace(/\s+/,'+'),'&r=json'].join(''),
					fullUrl = [submitUrl,queryParams].join('');

				h.$ajax('get',fullUrl,function(res){
					console.log('response is: ',JSON.parse(res));
					if(Array.isArray(JSON.parse(res).Search)){
						createMovieListHtml(JSON.parse(res).Search);

					} else {
						qs('#results .movie-list').innerHTML = '';
					}
				});
			}

		});

		// Search button animation.
		qs('#search input.submit').addEventListener('click',searchButtonAnimation);

	};

	/* The main function, which is the only function called when the page loads. */
	function main(){
		setHandlers();
		createFavoritesHtml();
	};

	window.onload = main;

})(helpers);