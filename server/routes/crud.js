var express = require('express'),
	router = express.Router(),
  fs = require('fs'),
  mockDatabase = __dirname + '/data.json';


router.get('/', function (req, res) {
  res.render('index', {});
});

/* CRUD operations */

// Delete a favorite item, :t is our query parameter
router.delete('/favorites/:t',function(req,res){

  var data = JSON.parse(fs.readFileSync(mockDatabase)),
    title = req.params.t,
    newData = data.filter(function(movie){
    return movie.Title != title;
  });
 
  fs.writeFile(mockDatabase, JSON.stringify(newData));

});

// Show our favorite movies
router.get('/favorites', function(req, res){

  var data = fs.readFileSync(mockDatabase);
  res.setHeader('Content-Type', 'application/json');
  res.send(data);

});

// Add to/Update our favorite movies
router.put('/favorites', function(req, res){

  if(!req.body){
    res.send("Error");
    return
  }
  
  var data = JSON.parse(fs.readFileSync(mockDatabase));
  data.push(req.body);
  

  fs.writeFile(mockDatabase, JSON.stringify(data));

  res.setHeader('Content-Type', 'application/json');
  res.send(data);

});



module.exports = router;