/* Modules */
var express = require('express')
var bodyParser = require('body-parser');
var app = express();
var fs = require('fs');
var path = require('path');
var port = process.env.PORT || 3000;

/* Set methods */
app.set('views', path.join( __dirname, '../client/views' ));
app.set('view engine', 'pug');

/* Express middleware */
app.use(express.static(path.join(__dirname, '../client/static')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// Router for views to render.
app.use('/',require('./routes/views'));

// CRUD routes.
app.use('/',require('./routes/crud'));

app.listen(port, function(){
  console.log("Listening on port 3000");
});